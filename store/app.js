export const state = () => ({
    city : 'Lille',
    weather : {
        main : 'Sunny',
        icon : 'sun',
        temp_crr : 0,
        humidity : 0,
        wind : '',
        pressure : 0,
        clouds : 0
    },
    forecast : []

})

export const mutations = {
    setupData(state, currWeather){
        state.city = currWeather.name;
        const des = currWeather.weather[0].description;
        state.weather.main = des.charAt(0).toUpperCase() + des.slice(1)
        state.weather.icon = translateIcons(currWeather.weather[0].icon)

        state.weather.temp_crr = ktoC(currWeather.main.temp);
        state.weather.humidity = currWeather.main.humidity;


        let windir = currWeather.wind.deg ? degresToCard(currWeather.wind.deg) : '';
        state.weather.wind = windir + ' ' +  Math.floor(currWeather.wind.speed * 3.6);

        state.weather.pressure = currWeather.main.pressure;
        state.weather.clouds = currWeather.clouds.all;


        state.forecast = [];
        for(let i = 0; i < 7; i++){
            const dt = new Date(currWeather.daily[i].dt * 1000);

            state.forecast.push({
                day : getDayName(dt.getDay()),
                temp : ktoC(currWeather.daily[i].temp.max),
                feel : ktoC(currWeather.daily[i].feels_like.day)
            })
        }
    }
}

function ktoC(k){
    return Math.floor(k - 273.5);
}

function getDayName(num){
    const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    return days[num];
}

function degresToCard(deg){
    const cards = ['ne', 'e', 'se', 's', 'sw', 'w' , 'nw']

    if(deg > 337.5 || deg <= 22.5) return 'n'

    let index = 0;
    for(let i = 22.5 ; i < 337.5; i += 45){
        let min = i;
        let max = i + 45;
        if(deg > min &  deg <= max) return cards[index]
        index++;
    }
}

function translateIcons(code){
    const cd = code.slice(0,2);
    switch(cd){
        case '01' : return 'sun'
        case '02' : return 'cloudy'
        case '03' : return 'cloud1'
        case '04' : return 'cloudy1'
        case '09' : return 'rainy1'
        case '10' : return 'rainy'
        case '11' : return 'lightning1'
        case '13' : return 'snowy2'
        case '50' : return 'weather2'
        default : return 'weather2'
    }
}